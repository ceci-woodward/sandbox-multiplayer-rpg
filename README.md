### Sandbox Multiplayer RPG

1. Immersion of the user is the top priority.
2. Enjoyment supersedes making realistic gameplay.
3. We should always aim for a low barrier of entry, while simultaneously having a high skill ceiling.
