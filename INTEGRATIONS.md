### Guild Chat

- Messages in-game should be shown in the discord chat
- Messages in the discord chat should be shown in-game

#### Supported Platforms

- Discord
- Slack